# To Do List Tracker

#### A sample webpage that allows the user to add various tasks to a To-Do list and manage them. Created on 1/15/2021 & Updated on 1/17/2021.

#### By: Janet Karpenske

##### This website uses React to allow the user to add tasks to a to do list, edit them, view them, and delete them.

### Link to Site on Github Pages
#### NA

## Setup/Installation Requirements

1. Download/Clone The Project from the repository
2. Navigate into main directory called 'to-do-list' and run command "npm install" in terminal.
3. To view on server run command "npm run start".

## React Component Diagram
#### NA

## Known Bugs
No Known Bugs

## Support & Contact Details
For additional information or to contact Janet Karpenske:
email: janetkarpenske@gmail.com
LinkedIn: Janet Karpenske

## Technologies Used
React, Redux, JavaScript, HTML, CSS, Bootstrap, JSX, Firestore

## Licensing
Copyright (c) 2020 **_{Janet Karpenske}_**

This software is licensed under MIT license.