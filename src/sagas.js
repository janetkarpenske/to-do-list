import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'


function* handleClickAsync() {
  yield put({type: 'SELECT_TASK_ASYNC'})
}
function* deleteTaskAsync() {
  yield put({type: 'DELETE_TASK_ASYNC'})
}
function* toggleFormAsync() {
  yield put({type: 'TOGGLE_FORM_ASYNC'})
}
//bug occurs when you click on edit and try to go back. Doesn't toggle form back off.
function* toggleEditAsync() {
  yield put({type: 'TOGGLE_EDIT_ASYNC'})
}
function* nullTaskAsync() {
  yield put({type: 'SET_TASK_NULL_ASYNC'})
}
export function* watchHandleClick() {
  console.log('Saga handleClick function reached!');
  yield takeEvery('SELECT_TASK', handleClickAsync);
  yield takeEvery('DELETE_TASK', deleteTaskAsync);
  yield takeEvery('TOGGLE_FORM', toggleFormAsync);
  yield takeEvery('TOGGLE_EDIT', toggleEditAsync);
  yield takeEvery('SET_TASK_NULL', nullTaskAsync);
}