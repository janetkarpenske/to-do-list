import React from 'react';
import PropTypes from 'prop-types';

function ReusableForm(props) {
  return (
    <React.Fragment>
      <form onSubmit = {props.formSubmissionHandler}>
        <input className="form-control" type='text' name='title' placeholder='Title' required /><br/>
        <textarea className="form-control" type='text' name='details' placeholder='Additional Details' /><br/>
        <button className='btn btn-success btn-md' type='submit'>{props.buttonText}</button><br/>
      </form>
    </React.Fragment>
  );
}

ReusableForm.propTypes = {
  formSubmissionHandler: PropTypes.func,
  buttonText: PropTypes.string
};

export default ReusableForm;