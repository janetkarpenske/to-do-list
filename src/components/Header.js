import React from 'react';

function Header () {

  return (
    <React.Fragment>
      <div className="center-align title">
        <h1 className="center-align">to do list tracker</h1>
        <h2 className="center-align">create and manage your tasks</h2>
      </div>
    </React.Fragment>
  );
}

export default Header;