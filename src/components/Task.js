import React from 'react';
import PropTypes from 'prop-types';

function Task (props) {
  return (
    <React.Fragment>
      <div className="hoverItem" onClick = {() => props.whenTaskIsClicked(props.id)}> 

        <h4><strong>{props.title}</strong></h4>
        <p>{props.details}</p>
      </div>
      <hr/>
    </React.Fragment>
  );
}

Task.propTypes = {
  title: PropTypes.string,
  details: PropTypes.string,
  id: PropTypes.string,
  whenTaskIsClicked: PropTypes.func
}

export default Task;