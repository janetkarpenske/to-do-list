import React from "react";
import Header from './Header';
import CompControl from './CompControl';
// import './../App.css';

function App() {
  return (
    <React.Fragment>
      <Header />
      <div className="container-fluid">
        <CompControl />
      </div>
    </React.Fragment>
  );
}

export default App;
