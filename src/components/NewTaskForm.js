import React from 'react';
import PropTypes from 'prop-types';
import ReusableForm from './ReusableForm';
import { useFirestore } from 'react-redux-firebase';

function NewTaskForm (props) {
  const firestore = useFirestore();

  function addTaskToFirestore(event) {
    event.preventDefault();
    props.onNewTaskCreation();

    return firestore.collection('tasks').add(
      {
        title: event.target.title.value,
        details: event.target.details.value,
        timeOpen: firestore.FieldValue.serverTimestamp()
      }
    );
  }

  return (
    <React.Fragment>
      <ReusableForm formSubmissionHandler={addTaskToFirestore}
      buttonText="Add Task"/>
    </React.Fragment>
  );
}

NewTaskForm.propTypes = {
  onNewTaskCreation: PropTypes.func,
  buttonText: PropTypes.string
};

export default NewTaskForm;