import React from 'react';
import TaskList from './TaskList';
import NewTaskForm from './NewTaskForm';
import EditTaskForm from './EditTaskForm';
import TaskDetails from './TaskDetails';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as a from './../actions/Index';
import { withFirestore } from 'react-redux-firebase';

class CompControl extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClickAsync = () => {
    const { dispatch } = this.props;
    if (this.props.selectedTask != null && this.props.editing) {
      const action1 = a.setTaskNull();
      dispatch(action1);
      const action2 = a.toggleEdit();
      dispatch(action2);
    } 
    else if (this.props.selectedTask != null) {
      const action1 = a.setTaskNull();
      dispatch(action1);
    } 
    else {
      const action = a.toggleForm();
      dispatch(action);
      }
  }
  handleAddingNewTaskToList = () => {
    const { dispatch } = this.props;
    const action = a.toggleForm();
    dispatch(action);
  }
  handleSelectingTask = (id) => {
    this.props.firestore.get({collection: 'tasks', doc: id}).then((task) => {
      const firestoreTask = {
        title: task.get('title'),
        details: task.get('details'),
        id: task.id
      }
    const { dispatch } = this.props;
    const action = a.selectTask(firestoreTask);
    dispatch(action);
    });
  }
  handleDeletingTask = (id) => {
    this.props.firestore.delete({collection: 'tasks', doc: id});
    const { dispatch } = this.props;
      const action = a.setTaskNull();
      dispatch(action);
  }

  handleEditClick = () => {
    const { dispatch } = this.props;
    const action = a.toggleEdit();
    dispatch(action);
  }
  handleEditingTaskInList = () => {
    const { dispatch } = this.props;
      const action = a.toggleEdit();
      dispatch(action);
      const action2 = a.setTaskNull();
      dispatch(action2);
  }
  render() {
    let currentlyVisibleState = null;
    let buttonText = null;
    if (this.props.editing) {
      currentlyVisibleState = <EditTaskForm task={this.props.selectedTask} onEditTask={this.handleEditingTaskInList}/>
      buttonText = "Back To List";
    }
    else if (this.props.selectedTask != null) {
      currentlyVisibleState = <TaskDetails task = {this.props.selectedTask} onClickingDelete = {this.handleDeletingTask} onClickingEdit = {this.handleEditClick} />
      buttonText="Back To List";
    }
    else if(this.props.formVisibleOnPage) {
      currentlyVisibleState = <NewTaskForm onNewTaskCreation={this.handleAddingNewTaskToList} />
      buttonText="Back To List";
    }
    else {
      currentlyVisibleState =  <TaskList taskList={this.props.masterTaskList} onTaskSelection={this.handleSelectingTask}/>;
      buttonText = "+ Add Task";
    }
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-6">
            {currentlyVisibleState}
            <button className="btn btn-dark btn-md" onClick={this.handleClickAsync}>{buttonText}</button>
          </div>
          <div className="col-md-6">
          {/* <TaskList taskList={this.props.masterTaskList} onTaskSelection={this.handleSelectingTask}/> */}
          </div>
        
        </div>
      </React.Fragment>
      );
  }
};

CompControl.propTypes = {
  masterTaskList: PropTypes.object,
  formVisibleOnPage: PropTypes.bool,
  editing: PropTypes.bool,
  selectedTask: PropTypes.object
};

const mapStateToProps = state => {
  return {
    masterTaskList: state.masterTaskList,
    formVisibleOnPage: state.formVisibleOnPage,
    editing: state.editing,
    selectedTask: state.selectedTask
  }
}

CompControl = connect(mapStateToProps)(CompControl);

export default withFirestore(CompControl);