import React from 'react';
import Task from './Task';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useFirestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase';

function TaskList(props){
  useFirestoreConnect([
    { collection: 'tasks' }
  ]);

  const tasks = useSelector(state => state.firestore.ordered.tasks);
  //const orderedTasks = tasks.orderBy("title");

  if (isLoaded(tasks)) {
  return (
    <React.Fragment>
      <h2 className="center-align">Your Tasks</h2>
      <div className="bord">
      {tasks.map((task) => {
        return <Task
        whenTaskIsClicked = {props.onTaskSelection }
        title={task.title}
        details={task.details}
        id={task.id}
        key={task.id}/>
      })}
      </div>
    </React.Fragment>
  );
  } else {
    return (
    <React.Fragment>
      <h4>loading...</h4>
    </React.Fragment>
    )
  }
}

TaskList.propTypes = {
  //taskList: PropTypes.object,
  onTaskSelection: PropTypes.func
}

export default TaskList;