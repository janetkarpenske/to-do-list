import React from 'react';
import PropTypes from 'prop-types';

function TaskDetails(props) {
  const { task, onClickingDelete, onClickingEdit } = props;

  return (
    <React.Fragment>

        <h3><strong>{task.title}</strong></h3><br/><br/>
        
        <h4>{task.details}</h4><br/><br/>

        {/* <button className="btn db btn-dark btn-md" onClick={ () => onMainClick() }>Back To Tasks</button> */}

        <button className="btn db btn-dark btn-md" onClick={ () => onClickingDelete(task.id) }>- Remove Task</button>
        <button className="btn db btn-dark btn-md" onClick={ () => onClickingEdit(task.id) }>Edit Task</button>

    </React.Fragment>
  );
}

TaskDetails.propTypes = {
  task: PropTypes.object,
  onClickingDelete: PropTypes.func,
  onClickingEdit: PropTypes.func
}
export default TaskDetails;