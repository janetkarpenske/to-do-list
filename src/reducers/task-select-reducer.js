import * as c from './../actions/actionTypes';

export default (state = null, action) => {
  switch (action.type) {
    case c.SELECT_TASK_ASYNC:
      const tempSelectedTask = action.firestoreTask
      return tempSelectedTask;

    case c.SET_TASK_NULL_ASYNC:
      state = null;
      return state;
    default:
      return state;
  }
};