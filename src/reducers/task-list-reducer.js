import * as c from './../actions/actionTypes';

export default (state = {}, action) => {
  const { id } = action;
  let tempState;
  switch (action.type) {
    case c.DELETE_TASK_ASYNC:
      tempState = {...state};
      delete tempState[id];
      return tempState;
    default:
      return state;
  }
}