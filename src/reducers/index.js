import formVisibleReducer from './form-visible-reducer';
import taskListReducer from './task-list-reducer';
import editingReducer from './editing-reducer';
import taskSelectReducer from './task-select-reducer';
import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';

const rootReducer = combineReducers({
  formVisibleOnPage: formVisibleReducer,
  masterTaskList: taskListReducer,
  editing: editingReducer,
  selectedTask: taskSelectReducer,
  firestore: firestoreReducer
});

export default rootReducer;