import * as c from './actionTypes';

export const deleteTask = id => ({
  type: c.DELETE_TASK_ASYNC,
  id
});
export const toggleForm = () => ({
  type: c.TOGGLE_FORM_ASYNC
});
export const toggleEdit = () => ({
  type: c.TOGGLE_EDIT_ASYNC
});
export const selectTask = (firestoreTask) => {
  return {
    type: c.SELECT_TASK_ASYNC,
    firestoreTask: firestoreTask
  };
};
export const setTaskNull = () => {
  return {
    type: c.SET_TASK_NULL_ASYNC
  };
};
